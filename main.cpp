#include <windows.h>
#include <DirectXMath.h>
#include <SimpleMath.h>
#include <SDL3/SDL.h>
#include <SDL_system.h>
#include <memory>
#include "DX.h"
#include "Shader.h"
#include "Mesh.h"
#include <vector>
#include <DDSTextureLoader.h>
#include "imgui.h"
#include "imgui_impl_sdl3.h"
#include "imgui_impl_dx11.h"
#include <d3d11_1.h>
#include "CB.h"
#include "Transform.h"
#include "Material.h"
#include "Quad.h"

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

SDL_Window *Window;


constexpr unsigned int WIDTH = 1360;
constexpr unsigned int HEIGHT = 720;


int main()
{
    try
    {
        bool should_quit = false;


        if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
        {
            SDL_Log("failed to load sdl");
            return -2;
        }

        Window = SDL_CreateWindow("directX", WIDTH, HEIGHT, 0);


        SDL_PropertiesID props = SDL_GetWindowProperties(Window);
        HWND hwnd = (HWND) SDL_GetProperty(props, SDL_PROP_WINDOW_WIN32_HWND_POINTER, nullptr);

        unique_ptr<DX> directX = make_unique<DX>(hwnd, WIDTH, HEIGHT);

        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO &io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
        ImGui_ImplSDL3_InitForD3D(Window);
        ImGui_ImplDX11_Init(directX->Device, directX->D3dContext);

        D3D11_INPUT_ELEMENT_DESC descs[] = {
                {
                        "POSITION",
                        0,
                        DXGI_FORMAT_R32G32B32_FLOAT,
                        0,
                        0,
                        D3D11_INPUT_PER_VERTEX_DATA,
                        0
                },
                {
                        "TEXCOORD",
                        0,
                        DXGI_FORMAT_R32G32_FLOAT,
                        0,
                        12,
                        D3D11_INPUT_PER_VERTEX_DATA,
                        0
                }
        };
        auto len = ARRAYSIZE(descs);


        auto shader = std::make_shared<Shader>(string("proj_Simple"), *directX, descs, len);

        auto vertices = make_unique<vector<VertexPosTex>>();
        vertices->push_back({XMFLOAT3(0.5f, 0.5f, 0.0f), XMFLOAT2(1.0, 1.0)});
        vertices->push_back({XMFLOAT3(0.5f, -0.5f, 0.0f), XMFLOAT2(1.0, 0.0)});
        vertices->push_back({XMFLOAT3(-0.5f, -0.5f, 0.0f), XMFLOAT2(0.0, 0.0)});
        vertices->push_back({XMFLOAT3(-0.5f, 0.5f, 0.0f), XMFLOAT2(0.0, 1.0)});

        auto indecies = make_unique<vector<unsigned int>>();
        indecies->push_back(0);
        indecies->push_back(1);
        indecies->push_back(2);
        indecies->push_back(0);
        indecies->push_back(2);
        indecies->push_back(3);


        auto mesh = std::make_shared<Mesh>(std::move(vertices), std::move(indecies), *directX);
        auto material = std::make_shared<Material<Transformation>>(shader);

        Quad quad(mesh, material);


        ID3D11Texture2D *tex;
        ID3D11ShaderResourceView *resourceView;

        HRESULT res = CreateDDSTextureFromFile(
                directX->Device, L"./textures/test.dds", reinterpret_cast<ID3D11Resource **>(&tex), &resourceView
        );
        if (FAILED(res))
        {
            PrintHresult(res);
            throw DirectXException("failed to create Texture");
        }

        ID3D11SamplerState *samplerState;

        D3D11_SAMPLER_DESC colorMapDesc;
        ZeroMemory(&colorMapDesc, sizeof(colorMapDesc));
        colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
        colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
        colorMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

        res = directX->Device->CreateSamplerState(&colorMapDesc, &samplerState);
        if (FAILED(res))
        {
            PrintHresult(res);
            throw DirectXException("failed to create sampler");
        }

        material->AddResourceView(resourceView);
        material->AddSampler(samplerState);

        D3D11_BUFFER_DESC perFrameCBuffer;
        ZeroMemory(&perFrameCBuffer, sizeof perFrameCBuffer);
        perFrameCBuffer.ByteWidth = sizeof(Transformation) * 2;
        perFrameCBuffer.Usage = D3D11_USAGE::D3D11_USAGE_DYNAMIC;
        perFrameCBuffer.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
        perFrameCBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
        perFrameCBuffer.MiscFlags = 0;
        perFrameCBuffer.StructureByteStride = 0;


        auto cb = Transformation();
        auto transform = new Transform();
        transform->SetWSPosition(Math::Vector3(0.5, 0.0, 0));
        cb.matGeo = transform->GetModelToWorld().Transpose();
        cb.matVP = Matrix::CreateLookAt(
                Vector3(0.0, 0.0, 0.5)
                , Vector3(0.0, 0, 0.0)
                , Vector3::Up
        );
        cb.matVP *= Matrix::CreatePerspective(16.0 / 9.0, 1, 0.3, 100);
        cb.matVP = cb.matVP.Transpose();

        PerShaderCB *perShader;

        if (auto m = quad.Material.lock())
        {
            m->AddCB(&cb, sizeof(Transformation), 16);
            perShader = new PerShaderCB(m->CB->NumberOfConstants());
            perShader->AddPerMaterialCB(*m->CB);
        }


        ID3D11Buffer *perFrameBuff;

        res = directX->Device->CreateBuffer(&perFrameCBuffer, nullptr, &perFrameBuff);
        if (FAILED(res))
        {
            PrintHresult(res);
            throw DirectXException("failed to create per frame buffer");
        }


        bool open = false;
        float clearColor[4] = {0.0f, 0.2f, 0.25f, 1.0f};
        Vector3 camPos = Vector3(0.0, 0.0, 0.5);
        Vector3 pos = Vector3(0.0, 0.0, 0.0);
        Vector3 rot = Vector3(0.0, 0.0, 0.0);
        Vector3 scale = Vector3(1, 1, 1);


        D3D11_MAPPED_SUBRESOURCE subRes;
        ZeroMemory(&subRes, sizeof subRes);
        directX->D3dContext->Map(perFrameBuff, 0, D3D11_MAP_WRITE_DISCARD, 0, &subRes);
        perShader->WriteChanges(&subRes, true);
        directX->D3dContext->Unmap(perFrameBuff, 0);

        while (!should_quit)
        {
            SDL_Event event;
            if (SDL_PollEvent(&event) > 0)
            {
                switch (event.type)
                {
                    case SDL_EVENT_QUIT:
                        should_quit = true;
                        break;
                    default:
                        ImGui_ImplSDL3_ProcessEvent(&event);
                }
            }
            ImGui_ImplDX11_NewFrame();
            ImGui_ImplSDL3_NewFrame();
            ImGui::NewFrame();
            //ImGui::ShowDemoWindow();



            ImGui::SetNextWindowPos(ImVec2(0, 0));
            if (ImGui::Begin(
                    "Shader", &open, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar
            ))
            {
                ImGui::ColorEdit4("Clear Color", clearColor);
                ImGui::DragFloat3("Camera Position", (float *) &camPos, 0.01);
                ImGui::DragFloat3("Object Position", (float *) &pos, 0.01);
                ImGui::DragFloat3("Object Rotation", (float *) &rot, 0.01);
                ImGui::DragFloat3("Object Scale", (float *) &scale, 0.01);


                ImGui::End();
            }
            else
            {
                ImGui::End();
            }

            quad.Transform->SetWSPosition(pos);
            quad.Transform->SetRotation(rot);
            quad.Transform->SetWSScale(scale);
            cb.matGeo = quad.Transform->GetModelToWorld().Transpose();
            if (auto m = quad.Material.lock())
            {
                m->UpdateCB(0, &cb);
                perShader->UpdateMaterialCB(*m->CB);
            }


            directX->D3dContext->Map(perFrameBuff, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &subRes);
            perShader->WriteChanges(&subRes);
            directX->D3dContext->Unmap(perFrameBuff, 0);

            directX->D3dContext->ClearRenderTargetView(directX->BackBufferTarget, clearColor);


            quad.Draw(*directX, perFrameBuff);

            ImGui::Render();
            ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
            directX->SwapChain->Present(0, 0);

        }

        SDL_DestroyWindow(Window);
        SDL_Quit();

        return 0;
    }
    catch (exception &e)
    {
        cout << e.what() << endl;
        return -2;
    }
}


