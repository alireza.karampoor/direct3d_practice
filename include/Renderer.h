//
// Created by HAVOK on 3/8/2024.
//

#ifndef DIRECTX_RENDERER_H
#define DIRECTX_RENDERER_H


#include "Transformation.h"
#include "Mesh.h"
#include "Material.h"
#include "DX.h"

class Renderer
{
public:
    Renderer(std::weak_ptr<Mesh> mesh, std::weak_ptr<Material<Transformation>> material);

    std::weak_ptr<Mesh> Mesh;
    std::weak_ptr<Material<Transformation>> Material;


    void Draw(const DX &directx, ID3D11Buffer *perFrame);
};


#endif //DIRECTX_RENDERER_H
