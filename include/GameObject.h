//
// Created by HAVOK on 3/1/2024.
//

#ifndef DIRECTX_GAMEOBJECT_H
#define DIRECTX_GAMEOBJECT_H

#include <memory>
#include "Transform.h"

class GameObject
{
public:
    GameObject();

    virtual ~GameObject() = default;

    std::shared_ptr<Transform> Transform;

};


#endif //DIRECTX_GAMEOBJECT_H
