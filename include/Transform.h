//
// Created by HAVOK on 3/1/2024.
//

#ifndef DIRECTX_TRANSFORM_H
#define DIRECTX_TRANSFORM_H

#include <SimpleMath.h>
#include <memory>

namespace Math = DirectX::SimpleMath;

class Transform
{
public:
    Transform();

    explicit Transform(std::weak_ptr<Transform> parent);

    Transform(Math::Vector3 &&Position, Math::Vector3 &&Rotation, Math::Vector3 &&Scale);

    virtual ~Transform() = default;

    Math::Matrix GetModelToWorld();

    void SetParent(std::weak_ptr<Transform> parent);

    void SetWSPosition(Math::Vector3 position);

    void SetWSRotation(Math::Vector3 rotation);

    void SetWSScale(Math::Vector3 scale);

    Math::Vector3 GetWSPosition();

    Math::Vector3 GetWSRotation();

    Math::Vector3 GetWSScale();

    void SetPosition(Math::Vector3 position);

    void SetRotation(Math::Vector3 rotation);

    void SetScale(Math::Vector3 scale);

    Math::Vector3 GetPosition();

    Math::Vector3 GetRotation();

    Math::Vector3 GetScale();

private:
    std::weak_ptr<Transform> _parent;
    Math::Vector3 _position;
    Math::Vector3 _rotation;
    Math::Vector3 _scale;

    Math::Matrix _modelToWorld;
    Math::Vector3 _wsPosition;
    Math::Vector3 _wsRotation;
    Math::Vector3 _wsScale;

};


#endif //DIRECTX_TRANSFORM_H
