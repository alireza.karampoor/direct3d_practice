//
// Created by HAVOK on 2/23/2024.
//

#ifndef DIRECTX_MATERIAL_H
#define DIRECTX_MATERIAL_H

#include <memory>

#include "Shader.h"
#include "CB.h"

template<typename T>
class Material
{
public:
    explicit Material(std::weak_ptr<Shader> shader);

    std::weak_ptr<Shader> Shader;
    std::unique_ptr<PerMaterialCB> CB;
    std::vector<ID3D11ShaderResourceView *> *ResourceViews;
    std::vector<ID3D11SamplerState *> *SamplerStates;

    void AddCB(T *cb, size_t size, unsigned int numConsts);

    void UpdateCB(unsigned int index, const T *cb);

    void AddResourceView(ID3D11ShaderResourceView *resource);

    void AddSampler(ID3D11SamplerState *state);
};

template<typename T>
void Material<T>::AddSampler(ID3D11SamplerState *state)
{
    SamplerStates->push_back(state);
}

template<typename T>
void Material<T>::AddResourceView(ID3D11ShaderResourceView *resource)
{
    ResourceViews->push_back(resource);
}

template<typename T>
Material<T>::Material(std::weak_ptr<class Shader> shader)
{
    Shader = std::move(shader);
    CB = std::make_unique<PerMaterialCB>();
    ResourceViews = new vector<ID3D11ShaderResourceView *>();
    SamplerStates = new vector<ID3D11SamplerState *>();
}

template<typename T>
void Material<T>::AddCB(T *cb, size_t size, unsigned int numConsts)
{
    CB->AddBuffer(cb, size, numConsts);
}

template<typename T>
void Material<T>::UpdateCB(unsigned int index, const T *cb)
{
    CB->UpdateBuffer(index, cb);
}

#endif //DIRECTX_MATERIAL_H
