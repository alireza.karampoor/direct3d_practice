//
// Created by HAVOK on 2/16/2024.
//

#ifndef DIRECTX_MESH_H
#define DIRECTX_MESH_H

#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include <memory>
#include "DX.h"
#include "vertex.h"

using namespace DirectX;
using namespace std;

class Mesh
{
public:
    vector<VertexPosTex> *Vertices;
    vector<unsigned int> *Indices;

    ID3D11Buffer *VertexBuffer;
    ID3D11Buffer *IndexBuffer;

    unsigned int Stride;
    unsigned int Offset;

    DXGI_FORMAT IndexFormat;

    Mesh(unique_ptr<vector<VertexPosTex>> vertices, unique_ptr<vector<unsigned int>> indecies, const DX &dx);

    ~Mesh();
};


#endif //DIRECTX_MESH_H
