//
// Created by HAVOK on 2/16/2024.
//

#ifndef DIRECTX_SHADER_H
#define DIRECTX_SHADER_H

#include <map>
#include <array>
#include <memory>
#include <d3d11.h>
#include "DX.h"
#include "Exceptions.h"
#include <d3dcompiler.h>
#include <sstream>
#include <windows.h>

using namespace std;

class Shader
{
public:
    Shader(const string &name, const DX &dx, const D3D11_INPUT_ELEMENT_DESC *descs, const unsigned int len);

    ~Shader();

    ID3D11VertexShader *VertexShader;
    ID3D11PixelShader *PixelShader;
    ID3D11InputLayout *InputLayout;

private:
    static map<const char *, shared_ptr<Shader>> *_shaders;

};


#endif //DIRECTX_SHADER_H
