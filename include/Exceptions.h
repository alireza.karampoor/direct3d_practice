//
// Created by HAVOK on 2/16/2024.
//

#ifndef DIRECTX_EXCEPTIONS_H
#define DIRECTX_EXCEPTIONS_H

#include <iostream>
#include <windows.h>

class DirectXException : public std::exception
{
public:
    DirectXException(const char *msg);
};

void PrintHresult(HRESULT hresult);

#endif //DIRECTX_EXCEPTIONS_H
