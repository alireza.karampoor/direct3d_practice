//
// Created by HAVOK on 2/28/2024.
//

#ifndef DIRECTX_CB_H
#define DIRECTX_CB_H

#include <vector>
#include <d3d11.h>
#include "SimpleMath.h"

using namespace std;


class Buffer
{
public:
    explicit Buffer(size_t init);

    ~Buffer();


protected:
    void AddElement(const void *buffer, size_t size);

    void *_buffers;
    size_t _buffersCap;
    size_t _buffersLen;
};


class PerMaterialCB : public Buffer
{
public:
    unsigned int NumBuffers;
    vector<unsigned int> *LocalOffsets;
    unsigned int GlobalOffset;
    vector<unsigned int> *NumConsts;

    PerMaterialCB();

    ~PerMaterialCB();


    void AddBuffer(const void *buffer, size_t size, unsigned int numConsts);

    void UpdateBuffer(unsigned int index, const void *buffer);

    unsigned int NumberOfConstants();

    size_t Size();

    void *Data();

private:
    unsigned int _lastOffset;

};

class PerShaderCB : public Buffer
{
public:


    explicit PerShaderCB(unsigned int numConsts);

    ~PerShaderCB();

    void AddPerMaterialCB(PerMaterialCB &cb);

    void UpdateMaterialCB(PerMaterialCB &cb);

    void WriteChanges(const D3D11_MAPPED_SUBRESOURCE *subresource, bool all = false);


private:
    unsigned int _lastIndex;
    vector<unsigned int> *_starts;
    vector<size_t> *_sizes;

};

#endif //DIRECTX_CB_H
