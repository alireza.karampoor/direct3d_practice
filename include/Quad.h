//
// Created by HAVOK on 3/7/2024.
//

#ifndef DIRECTX_QUAD_H
#define DIRECTX_QUAD_H


#include "GameObject.h"
#include "Mesh.h"
#include "Material.h"
#include "Transformation.h"
#include "Renderer.h"

typedef Material<Transformation> Mat;

class Quad : public GameObject, public Renderer
{
public:
    Quad(std::weak_ptr<class Mesh> mesh, std::weak_ptr<Mat> material);


};


#endif //DIRECTX_QUAD_H
