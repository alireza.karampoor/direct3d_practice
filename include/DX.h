//
// Created by HAVOK on 2/16/2024.
//

#ifndef DIRECTX_DX_H
#define DIRECTX_DX_H

#include <windows.h>
#include <d3d11.h>
#include <d3d11_1.h>


class DX
{
public:
    DX(HWND windowHandle, unsigned int width, unsigned int height);

    DX(const DX &dx) = delete;

    ~DX();

    unsigned int Width;
    unsigned int Height;

    ID3D11Device1 *Device;
    ID3D11DeviceContext1 *D3dContext;
    IDXGISwapChain *SwapChain;
    ID3D11RenderTargetView *BackBufferTarget;
};


#endif //DIRECTX_DX_H
