//
// Created by HAVOK on 2/23/2024.
//

#ifndef DIRECTX_VERTEX_H
#define DIRECTX_VERTEX_H

#include <DirectXMath.h>

using namespace DirectX;

struct VertexPosTex
{
    XMFLOAT3 Position;
    XMFLOAT2 UV;
};


#endif //DIRECTX_VERTEX_H
