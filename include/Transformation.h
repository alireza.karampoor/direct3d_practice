//
// Created by HAVOK on 3/7/2024.
//

#ifndef DIRECTX_TRANSFORMATION_H
#define DIRECTX_TRANSFORMATION_H

#include "SimpleMath.h"

namespace Math = DirectX::SimpleMath;

struct alignas(256) Transformation
{
    Math::Matrix matGeo;
    Math::Matrix matVP;
};

#endif //DIRECTX_TRANSFORMATION_H
