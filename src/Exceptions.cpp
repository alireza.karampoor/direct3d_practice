//
// Created by HAVOK on 2/16/2024.
//

#include "Exceptions.h"


DirectXException::DirectXException(const char *msg) : exception(msg)
{

}

void PrintHresult(HRESULT hresult)
{
    printf("0x%08lx\n", hresult);
}
