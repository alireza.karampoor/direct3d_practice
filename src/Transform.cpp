//
// Created by HAVOK on 3/1/2024.
//

#include "Transform.h"

#include <utility>

Transform::Transform()
{
    _position = Math::Vector3::Zero;
    _rotation = Math::Vector3::Zero;
    _scale = Math::Vector3(1, 1, 1);
    _modelToWorld = Math::Matrix::Identity;
}

Transform::Transform(std::weak_ptr<Transform> parent)
{
    _position = Math::Vector3::Zero;
    _rotation = Math::Vector3::Zero;
    _scale = Math::Vector3(1, 1, 1);
    _modelToWorld = Math::Matrix::Identity;
    _parent = std::move(parent);
}

Transform::Transform(Math::Vector3 &&Position, Math::Vector3 &&Rotation, Math::Vector3 &&Scale)
{
    Position = Position;
    Rotation = Rotation;
    Scale = Scale;
    auto mat = Math::Matrix::CreateScale(Scale);
    mat *= Math::Matrix::CreateFromYawPitchRoll(Rotation);
    mat *= Math::Matrix::CreateTranslation(Position);
    _modelToWorld = mat;

}

Math::Matrix Transform::GetModelToWorld()
{

    if (auto parent = _parent.lock())
    {
        auto parentMat = Math::Matrix::Identity;
        parentMat = parent->GetModelToWorld();
        auto mat = Math::Matrix::CreateScale(_scale);
        mat *= Math::Matrix::CreateFromYawPitchRoll(_rotation);
        mat *= Math::Matrix::CreateTranslation(_position);

        parentMat *= mat;
        _modelToWorld = parentMat;
        return _modelToWorld;
    }
    else
    {
        auto mat = Math::Matrix::Identity;
        mat *= Math::Matrix::CreateScale(_scale);
        mat *= Math::Matrix::CreateFromYawPitchRoll(_rotation);
        mat *= Math::Matrix::CreateTranslation(_position);

        return mat;
    }
}

void Transform::SetParent(std::weak_ptr<Transform> parent)
{
    _parent = std::move(parent);
}

void Transform::SetWSPosition(Math::Vector3 position)
{
    _wsPosition = position;

    if (auto p = _parent.lock())
    {
        auto parentMat = p->GetModelToWorld();
        auto local = Math::Matrix::CreateTranslation(position) * parentMat.Invert();
        _position = local.Translation();
    }
    else
    {
        _position = _wsPosition;
    }
}

void Transform::SetWSRotation(Math::Vector3 rotation)
{
    _wsRotation = rotation;

    if (auto p = _parent.lock())
    {
        auto parentMat = p->GetModelToWorld();
        auto local = Math::Matrix::CreateFromYawPitchRoll(rotation) * parentMat.Invert();
        _rotation = local.ToEuler();
    }
    else
    {
        _rotation = _wsRotation;
    }
}

void Transform::SetWSScale(Math::Vector3 scale)
{
    _wsScale = scale;

    if (auto p = _parent.lock())
    {
        auto parentMat = p->GetModelToWorld();
        auto local = Math::Matrix::CreateScale(scale) * parentMat.Invert();
        Math::Vector3 pos, s;
        Math::Quaternion r;
        local.Decompose(s, r, pos);
        _scale = s;
    }
    else
    {
        _scale = _wsScale;
    }
}

Math::Vector3 Transform::GetWSPosition()
{
    return _wsPosition;
}

Math::Vector3 Transform::GetWSRotation()
{
    return _wsRotation;
}

Math::Vector3 Transform::GetWSScale()
{
    return _wsScale;
}

void Transform::SetPosition(Math::Vector3 position)
{
    _position = position;
    auto mat = GetModelToWorld();
    _wsPosition = mat.Translation();
}

void Transform::SetRotation(Math::Vector3 rotation)
{
    _rotation = rotation;
    auto mat = GetModelToWorld();
    _wsRotation = mat.ToEuler();
}

void Transform::SetScale(Math::Vector3 scale)
{
    _scale = scale;
    auto mat = GetModelToWorld();

    Math::Vector3 p, s;
    Math::Quaternion r;
    mat.Decompose(s, r, p);
    _wsScale = s;
}

Math::Vector3 Transform::GetPosition()
{
    return _position;
}

Math::Vector3 Transform::GetRotation()
{
    return _rotation;
}

Math::Vector3 Transform::GetScale()
{
    return _scale;
}


