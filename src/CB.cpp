//
// Created by HAVOK on 2/28/2024.
//

#include "CB.h"
#include "Exceptions.h"

Buffer::Buffer(size_t init)
{
    _buffersLen = 0;
    _buffers = _aligned_malloc(init, 16);
    _buffersCap = init;
}

Buffer::~Buffer()
{
    _aligned_free(_buffers);
}

void Buffer::AddElement(const void *buffer, size_t size)
{
    if (size > _buffersCap - _buffersLen)
    {
        _buffers = _aligned_realloc(_buffers, (size + _buffersLen) * 2, 16);
        _buffersCap = (size + _buffersLen) * 2;
    }
    memcpy(((char *) _buffers) + _buffersLen, buffer, size);
    _buffersLen += size;
}


PerMaterialCB::PerMaterialCB() : Buffer(256)
{
    GlobalOffset = 0;
    _lastOffset = 0;
    NumBuffers = 0;
    LocalOffsets = new vector<unsigned int>();
    NumConsts = new vector<unsigned int>();

}

PerMaterialCB::~PerMaterialCB()
{
    delete LocalOffsets;
    delete NumConsts;
}

void PerMaterialCB::AddBuffer(const void *buffer, size_t size, unsigned int numConsts)
{
    if (size % 256 != 0)
    {
        throw DirectXException("buffer should be 256 byte aligned");
    }
    if (numConsts % 16 != 0)
    {
        throw DirectXException("numConsts should be multiple of 16");
    }

    AddElement(buffer, size);
    LocalOffsets->push_back(_lastOffset);
    _lastOffset += numConsts;
    NumBuffers++;
    NumConsts->push_back(numConsts);
}

void PerMaterialCB::UpdateBuffer(unsigned int index, const void *buffer)
{
    unsigned int begin = (*LocalOffsets)[index] * 16;
    size_t size = (*NumConsts)[index] * 16;
    memcpy((char *) _buffers + begin, buffer, size);

}

size_t PerMaterialCB::Size()
{
    return _buffersLen;
}

void *PerMaterialCB::Data()
{
    return _buffers;
}

unsigned int PerMaterialCB::NumberOfConstants()
{
    unsigned int acc = 0;
    for (unsigned int NumConst: *NumConsts)
    {
        acc += NumConst;
    }
    return acc;
}


PerShaderCB::PerShaderCB(unsigned int numConsts) : Buffer(numConsts * 16)
{
    _lastIndex = 0;
    _starts = new vector<unsigned int>();
    _sizes = new vector<size_t>();

}

PerShaderCB::~PerShaderCB()
{
    delete _starts;
    delete _sizes;
}

void PerShaderCB::AddPerMaterialCB(PerMaterialCB &cb)
{
    cb.GlobalOffset = _lastIndex;
    _lastIndex += cb.Size() / 16;
    AddElement(cb.Data(), cb.Size());
}

void PerShaderCB::UpdateMaterialCB(PerMaterialCB &cb)
{
    unsigned int offset = cb.GlobalOffset * 16;
    _starts->push_back(offset);
    _sizes->push_back(cb.Size());
    memcpy((char *) _buffers + offset, cb.Data(), cb.Size());
}

void PerShaderCB::WriteChanges(const D3D11_MAPPED_SUBRESOURCE *subresource, bool all)
{
    if (all)
    {
        memcpy(subresource->pData, _buffers, _buffersLen);
        return;
    }

    for (int i = 0; i < _starts->size(); ++i)
    {
        auto offset = (*_starts)[i];
        auto size = (*_sizes)[i];
        auto dst = (char *) subresource->pData + offset;
        auto src = (char *) _buffers + offset;
        memcpy(dst, src, size);
    }
    _starts->clear();
    _sizes->clear();
}


