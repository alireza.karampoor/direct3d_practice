//
// Created by HAVOK on 3/8/2024.
//

#include "Renderer.h"

typedef Material<Transformation> Mat;

Renderer::Renderer(std::weak_ptr<class Mesh> mesh, std::weak_ptr<Mat> material)
{
    Mesh = std::move(mesh);
    Material = std::move(material);
}

void Renderer::Draw(const DX &directX, ID3D11Buffer *perFrame)
{
    if (auto m = Material.lock())
    {
        if (auto mesh = Mesh.lock())
        {
            if (auto shader = m->Shader.lock())
            {
                directX.D3dContext->IASetInputLayout(shader->InputLayout);
                directX.D3dContext->IASetIndexBuffer(mesh->IndexBuffer, mesh->IndexFormat, 0);
                directX.D3dContext->IASetVertexBuffers(0, 1, &mesh->VertexBuffer, &mesh->Stride, &mesh->Offset);
                directX.D3dContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
                directX.D3dContext->VSSetShader(shader->VertexShader, nullptr, 0);
                directX.D3dContext->PSSetShader(shader->PixelShader, nullptr, 0);

                directX.D3dContext->PSSetShaderResources(0, m->ResourceViews->size(), m->ResourceViews->data());
                directX.D3dContext->PSSetSamplers(0, m->SamplerStates->size(), m->SamplerStates->data());

                unsigned int numConsts = m->CB->NumberOfConstants();
                directX.D3dContext->VSSetConstantBuffers1(0, 1, &perFrame, &m->CB->GlobalOffset, &numConsts);
                directX.D3dContext->DrawIndexed(mesh->Indices->size(), 0, 0);
            }
        }
    }

}
