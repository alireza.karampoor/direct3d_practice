//
// Created by HAVOK on 2/16/2024.
//

#include "Shader.h"

map<const char *, shared_ptr<Shader>> *Shader::_shaders = new map<const char *, shared_ptr<Shader>>;

Shader::Shader(const string &name, const DX &dx, const D3D11_INPUT_ELEMENT_DESC *descs, const unsigned int len)
{
    ostringstream vsPath;
    vsPath << "./shaders/" << name << "VS.hlsl";
    auto vsPathStr = vsPath.str();

    ostringstream psPath;
    psPath << "./shaders/" << name << "PS.hlsl";

    auto psPathStr = psPath.str();

    if (_shaders == nullptr)
    {
        _shaders = new map<const char *, shared_ptr<Shader>>;
    }

    ID3DBlob *vsBuffer = nullptr;
    ID3DBlob *errBuffer = nullptr;
    HRESULT result = 0;

    auto wstr = wstring(vsPathStr.begin(), vsPathStr.end());
    LPCWSTR lpcwstr = wstr.c_str();

    result = D3DCompileFromFile(
            lpcwstr
            , nullptr
            , nullptr
            , "VS_Main"
            , "vs_4_0"
            , D3DCOMPILE_DEBUG
            , 0
            , &vsBuffer
            , &errBuffer
    );
    if (FAILED(result))
    {
        if (errBuffer != nullptr)
        {
            auto err = (char *) errBuffer->GetBufferPointer();
            ostringstream str;
            str << "failed to compile vertex shader " << endl;
            errBuffer->Release();
            throw DirectXException(str.str().c_str());
        }
        ostringstream str;
        str << "failed to compile vertex shader " << endl;
        errBuffer->Release();
        throw DirectXException(str.str().c_str());
    }

    result = dx.Device
               ->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), nullptr, &VertexShader);

    if (FAILED(result))
    {
        if (vsBuffer)
        {
            vsBuffer->Release();
        }
        throw DirectXException("couldn't create vertex shader");
    }

    ID3DBlob *psBuffer;
    errBuffer = nullptr;

    wstr = wstring(psPathStr.begin(), psPathStr.end());
    lpcwstr = wstr.c_str();

    result = D3DCompileFromFile(
            lpcwstr
            , nullptr
            , nullptr
            , "PS_Main"
            , "ps_4_0"
            , 0
            , 0
            , &psBuffer
            , &errBuffer
    );
    if (FAILED(result))
    {
        if (errBuffer != nullptr)
        {
            auto err = (char *) errBuffer->GetBufferPointer();
            ostringstream str;
            str << "failed to compile pixel shader " << err << endl;
            errBuffer->Release();
            throw DirectXException(str.str().c_str());
        }
        throw DirectXException("failed to compile pixel shader");
    }

    result = dx.Device
               ->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(), nullptr, &PixelShader);

    if (FAILED(result))
    {
        if (psBuffer)
        {

            psBuffer->Release();
        }

        if (vsBuffer)
        {
            vsBuffer->Release();
        }
        throw DirectXException("couldn't create pixel shader");
    }

    (*_shaders)[name.c_str()] = shared_ptr<Shader>(this);

    result = dx.Device
               ->CreateInputLayout(descs, len, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &InputLayout);
    if (FAILED(result))
    {

        if (psBuffer)
        {
            psBuffer->Release();
        }

        if (vsBuffer)
        {
            vsBuffer->Release();
        }
        PrintHresult(result);
        throw DirectXException("couldn't create input layout");
    }

    if (psBuffer)
    {
        psBuffer->Release();
    }

    if (vsBuffer)
    {
        vsBuffer->Release();
    }
}

Shader::~Shader()
{
    if (VertexShader)
    {
        VertexShader->Release();
        VertexShader = nullptr;
    }
    if (PixelShader)
    {
        PixelShader->Release();
        PixelShader = nullptr;
    }
    if (InputLayout)
    {
        InputLayout->Release();
        InputLayout = nullptr;
    }

}

