//
// Created by HAVOK on 2/16/2024.
//
#include <iostream>
#include <string>
#include <sstream>
#include "DX.h"
#include "Exceptions.h"

DX::DX(HWND windowHandle, unsigned int width, unsigned int height)
{
    Height = height;
    Width = width;

    D3D_DRIVER_TYPE driverTypes[] = {
            D3D_DRIVER_TYPE_HARDWARE
    };
    unsigned int numDrivers = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] = {
            D3D_FEATURE_LEVEL_11_1
    };
    unsigned int numFeatLevels = ARRAYSIZE(featureLevels);

    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
    swapChainDesc.BufferCount = 1;
    swapChainDesc.BufferDesc.Width = Width;
    swapChainDesc.BufferDesc.Height = Height;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.OutputWindow = windowHandle;
    swapChainDesc.Windowed = true;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;


    unsigned int creationFlags = 0;

    creationFlags |= D3D11_CREATE_DEVICE_DEBUG;


    HRESULT result;
    for (int i = 0; i < numDrivers; ++i)
    {

        result = D3D11CreateDeviceAndSwapChain(
                nullptr
                , driverTypes[i]
                , nullptr
                , creationFlags
                , featureLevels
                , numFeatLevels
                , D3D11_SDK_VERSION
                , &swapChainDesc
                , &SwapChain
                , reinterpret_cast<ID3D11Device **>(&Device)
                , nullptr
                , reinterpret_cast<ID3D11DeviceContext **>(&D3dContext)
        );

        if (SUCCEEDED(result))
        {
            break;
        }
    }
    if (FAILED(result))
    {
        std::ostringstream str;
        str << "failed to create Direct3D device" << result << std::endl;
        std::string msg = str.str();
        throw DirectXException(msg.c_str());
    }


    ID3D10Texture2D *backBufferTexture;

    result = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **) &backBufferTexture);
    if (FAILED(result))
    {
        std::ostringstream str;
        str << "failed to get back buffer" << result << std::endl;
        std::string msg = str.str();
        throw DirectXException(msg.c_str());
    }

    result = Device->CreateRenderTargetView(
            reinterpret_cast<ID3D11Resource *>(backBufferTexture), nullptr, &BackBufferTarget
    );
    if (FAILED(result))
    {
        std::ostringstream str;
        str << "failed to create render target" << result << std::endl;
        std::string msg = str.str();
        throw DirectXException(msg.c_str());
    }

    if (backBufferTexture)
    {
        backBufferTexture->Release();
    }

    D3dContext->OMSetRenderTargets(1, &BackBufferTarget, nullptr);

    D3D11_VIEWPORT viewport;
    viewport.Width = static_cast<float>(Width );
    viewport.Height = static_cast<float>(Height);
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;
    viewport.TopLeftX = 0.0f;
    viewport.TopLeftY = 0.0f;

    D3dContext->RSSetViewports(1, &viewport);
}

DX::~DX()
{
    BackBufferTarget->Release();
    D3dContext->Release();
    Device->Release();
    SwapChain->Release();
}
