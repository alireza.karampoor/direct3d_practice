//
// Created by HAVOK on 2/16/2024.
//

#include "Mesh.h"
#include "DX.h"
#include "Exceptions.h"

using namespace DirectX;

Mesh::Mesh(unique_ptr<vector<VertexPosTex>> vertices, unique_ptr<vector<unsigned int>> indecies, const DX &dx)
{
    Stride = sizeof(VertexPosTex);
    Offset = 0;

    IndexFormat = DXGI_FORMAT::DXGI_FORMAT_R32_UINT;

    Vertices = vertices.release();
    Indices = indecies.release();

    D3D11_BUFFER_DESC vertexDesc;
    ZeroMemory(&vertexDesc, sizeof(vertexDesc));
    vertexDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexDesc.ByteWidth = sizeof(VertexPosTex) * Vertices->size();

    D3D11_SUBRESOURCE_DATA resourceData;
    ZeroMemory(&resourceData, sizeof(resourceData));
    resourceData.pSysMem = Vertices->data();

    HRESULT result = dx.Device->CreateBuffer(
            &vertexDesc, &resourceData, &VertexBuffer
    );
    if (FAILED(result))
    {
        PrintHresult(result);
        throw DirectXException("failed to create vertex buffer");
    }

    D3D11_BUFFER_DESC indexDesc;
    ZeroMemory(&indexDesc, sizeof(indexDesc));
    indexDesc.Usage = D3D11_USAGE_DEFAULT;
    indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexDesc.ByteWidth = sizeof(unsigned int) * Indices->size();

    D3D11_SUBRESOURCE_DATA indexData;
    ZeroMemory(&indexData, sizeof(indexData));
    indexData.pSysMem = Indices->data();


    result = dx.Device->CreateBuffer(&indexDesc, &indexData, &IndexBuffer);
    if (FAILED(result))
    {
        PrintHresult(result);
        throw DirectXException("failed to create indecies buffer");
    }

}

Mesh::~Mesh()
{
    delete Vertices;
    delete Indices;

    if (VertexBuffer)
    {
        VertexBuffer->Release();
    }
    if (IndexBuffer)
    {
        IndexBuffer->Release();
    }

}
