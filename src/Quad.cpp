//
// Created by HAVOK on 3/7/2024.
//

#include "Quad.h"
#include "CB.h"


Quad::Quad(std::weak_ptr<struct Mesh> mesh, std::weak_ptr<Mat> material) : Renderer(
        std::move(mesh)
        , std::move(material))
{

}
