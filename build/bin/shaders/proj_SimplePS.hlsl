Texture2D colorMap_ : register( t0 );
SamplerState colorSampler_ : register( s0 );

struct PSInput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
	float2 UV: TEXCOORD0;
};

float4 PS_Main(PSInput pin) : SV_TARGET
{
	return colorMap_.Sample( colorSampler_, pin.UV );
}
