cbuffer cbPerFrame : register(b0)
{
	
	float4x4 matGeo;
	float4x4 matVP;
};

struct VSInput
{
	float3 Position : POSITION;
	float3 UV : TEXCOORD0;
};

struct VSOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
	float2 UV: TEXCOORD0;
};

VSOutput VS_Main(VSInput vin)
{
	VSOutput vout = (VSOutput)0;
	
	vout.Position = mul(float4(vin.Position, 1.0f),matGeo);
	vout.Position = mul(vout.Position,matVP);
	
	vout.Color = 1.0;
	vout.UV = float2(vin.UV.x,1.0 - vin.UV.y);
	return vout;
}